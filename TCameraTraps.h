/*
 * TCameraTrap.h
 *
 *  Created on: Dec 15, 2018
 *      Author: phaentu
 */

#ifndef TCAMERATRAPS_H_
#define TCAMERATRAPS_H_

#include "stringFunctions.h"
#include "TRandomGenerator.h"
#include "TTimeIntervals.h"
#include <time.h>
#include "gzstream.h"
#include "TEnvVar.h"
#include "TCameraTrapfile.h"
#include <math.h>

//--------------------------------------------
// TCameraTrap
//--------------------------------------------
class TCameraTrap{
private:
	TEnvVar lambdaBar;
	TEnvVar p;
	time_t start;
	time_t end;

	//data & likelihood
	int numObservationIntervals;
	uint8_t* rawObervationCounts;
	time_t startOfFirstInterval;
	time_t endOfLastInterval;
	bool hasData;
	uint8_t** data; //these are counts of 0/1 observations across days per observation interval
	int*** data_sums;
	double LL, LL_old;

	void init();
	void write(const std::string & name, gz::ogzstream & out, TEnvVar & envVar);
	int simulateObservationsInterval(const std::string & name, const time_t & intervalStart, const double & intervalLength, const double & lambda, gz::ogzstream & out, TRandomGenerator* randomGenerator);

public:
	TCameraTrap();
	TCameraTrap(const time_t Start, const time_t End);
	~TCameraTrap();

	void initialize(const time_t & Start, const time_t & End);
	void setEnvironmentalVariables_lambda(std::vector<double> & Env, TEnvironment* env_lambda);
	void setEnvironmentalVariables_p(std::vector<double> & Env, TEnvironment* env_p);
	bool hasEnvironmentalVariables_lambda(){ if(lambdaBar.size() > 0) return true; else return false; };
	bool hasEnvironmentalVariables_p(){ if(p.size() > 0) return true; else return false; };
	time_t getStart(){ return start; };
	time_t getEnd(){ return end; };

	//environment
	void addToMeanVar(TMeanVar* mv_lambda, TMeanVar* mv_p);
	void standardizeEnv(TMeanVar* mv_lambda, TMeanVar* mv_p);
	void updateLambdaBar(TEnvironment* env);
	void updateP(TEnvironment* env);
	double calcLogLikelihood(TTimeIntervals & intervals);
	void resetLambdaBar();
	void resetP();
	void write(const std::string & name, gz::ogzstream & out_lambda, gz::ogzstream & out_p);

	//observations
	void initializeDataStorage(TTimeIntervals & intervals);
	bool addObservation(time_t time, TTimeIntervals & intervals);
	void compileData(TTimeIntervals & intervals);
	void writeData(const std::string & name, gz::ogzstream & out, TTimeIntervals & intervals);
	int simulateObservations(const std::string & name, TTimeIntervals & intervals, gz::ogzstream & out, TRandomGenerator* randomGenerator);
	int getNumObservationIntervals(){ return numObservationIntervals; };
	int getTotalObservations();
};

//--------------------------------------------
// TCameraTraps
//--------------------------------------------
class TCameraTraps{
private:
	TTimeIntervals* intervals;
	TEnvironment_lambda* env_lambda;
	TEnvironment_p* env_p;

	int numTraps;
	std::map<std::string, TCameraTrap*> traps; //mapping names to traps

	double LL, LL_old;

	void resetLambdaBar();
	void resetP();
	void openFileForReading(std::istream* in, const std::string & filename, std::string tag, TLog* logfile);
	int readHeaderOFCameraTrapFile(std::istream* in, std::vector<std::string> & envNames);

public:
	TCameraTraps(TEnvironment_lambda* Env_lambda, TEnvironment_p* Env_p, TTimeIntervals* Intervals);
	~TCameraTraps();

	bool updateLambdaBar(TRandomGenerator* randomGenerator);
	bool updateP(TRandomGenerator* randomGenerator);
	bool updateK(TRandomGenerator* randomGenerator);
	double update();
	double getLL(){ return LL; };

	void readTraps(const std::string & filename_lambda, const std::string & filename_p, TLog* logfile);
	void simulateTraps(int NumTraps, int Duration, TRandomGenerator* randomGenerator);
	void normalizeEnvVariables(std::string meanVarFileName_lambda, std::string meanVarFileName_p, TLog* logfile);

	void readObservations(const std::string & filename, TLog* logfile);
	int simulateObservations(std::string outname, TRandomGenerator* randomGenerator);
	void writeTraps(const std::string filename_lambda, const std::string filename_p);
	double getFractionOfIntervalsWithObservations();
};


#endif /* TCAMERATRAPS_H_ */
