/*
 * TEstimator.h
 *
 *  Created on: Dec 16, 2018
 *      Author: phaentu
 */

#ifndef TESTIMATOR_H_
#define TESTIMATOR_H_

#include "TLog.h"
#include "TParameters.h"
#include "TTimeIntervals.h"
#include "TCameraTraps.h"
#include <algorithm>

class TEstimator{
private:
	TLog* logfile;
	TRandomGenerator* randomGenerator;
	TEnvironment_lambda env_lambda;
	TEnvironment_p env_p;
	TTimeIntervals* intervals;
	bool intervalsInitialized;
	TCameraTraps* traps;
	bool trapsInitialized;

	std::string trapFileName_lambda;
	std::string trapFileName_p;
	std::string observationFileName;

	gz::ogzstream mcmcOut_K;
	gz::ogzstream mcmcOut_lambda;
	gz::ogzstream mcmcOut_p;

	std::vector<TPrior*> priors;
	bool estimatePi;

	void initializeIntervals(TParameters & params);
	void initializeTraps();
	void initializePriors(TParameters & params);
	void guessInitialParameters();
	void guessInitialA(TEnvironment* env, std::string progressString);
	double estimateAPeakFinder(TEnvironment* env, std::vector<double> & A, int index);
	void runMCMC(TParameters & params);
	void runBurnin(int len);
	void runMCMC(int len, int thinning);
	void runMCMCIteration();
	void reportAcceptanceRates();
	void openMCMCOutputFile(gz::ogzstream & file, std::string filename);
	void openMCMCOutputFiles(std::string & out);
	void writeCurrentParameters();

public:
	TEstimator(TLog* Logfile, TRandomGenerator* RandomGenerator);
	~TEstimator();

	void runEstimation(TParameters & params);
};



#endif /* TESTIMATOR_H_ */
