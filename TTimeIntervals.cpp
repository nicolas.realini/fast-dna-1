/*
 * TTimeIntervals.cpp
 *
 *  Created on: Dec 15, 2018
 *      Author: phaentu
 */


#include "TTimeIntervals.h"

TTimeIntervals::TTimeIntervals(){
	numActivityIntervals_nh = 0;
	numObservationIntervals_n = 0;
	activityLength = 0.0;
	observationlength = 0.0;
	numObservationPerActivityInterval = 0;
	numSecondsPerDay = 24 * 60 * 60;
	referenceDate = 0;

	K = NULL;
	K_scaled = NULL;
	observationToActivityMap = NULL;
	shift = 0;
	shift_old = 0;
	numShiftUpdates = 0;
	numShiftUpdatesAccepted = 0;

	curK = 0;
	initialized = false;
	logPriorProposalRatio = 0.0;
};

TTimeIntervals::TTimeIntervals(int NumActivityIntervals, int NumObsPerInterval, int Shift){
	numSecondsPerDay = 24 * 60 * 60;
	initialize(NumActivityIntervals, NumObsPerInterval, Shift);
};

TTimeIntervals::TTimeIntervals(TParameters & params, TLog* logfile){
	logfile->startIndent("Initializing intervals:");
	numSecondsPerDay = 24 * 60 * 60;
	int numActivityIntervals = params.getParameterIntWithDefault("numActivityIntervals", 8); //3 hour intervals

	//make sure #seconds / day is a multiple of #activity intervals, else adjust
	if(numSecondsPerDay % numActivityIntervals != 0){
		std::string report = "Adjusting numActivityIntervals from " + toString(numActivityIntervals) + " to ";

		//increase until it is a multiple
		while(numSecondsPerDay % numActivityIntervals != 0)
			++numActivityIntervals;
		logfile->list(report + toString(numActivityIntervals) + " so that the total number of seconds / day (86400) is a multiple of it.");
	}

	int numObsPerInterval = params.getParameterIntWithDefault("numObsPerInterval", 12); //15 minute intervals, 12 per 3 hour interval
	if(numSecondsPerDay % (numActivityIntervals*numObsPerInterval) != 0){
		std::string report = "Adjusting numActivityIntervals from " + toString(numObsPerInterval) + " to ";

		//increase until it is a multiple
		while(numSecondsPerDay % (numActivityIntervals*numObsPerInterval) != 0)
			++numObsPerInterval;
		logfile->list(report + toString(numObsPerInterval) + " so that the total number of seconds / day (86400) is a multiple of the total number of observation intervals per day.");
	}

	int Shift = params.getParameterIntWithDefault("shift", 0); //15 minute intervals
	if(Shift < 0 || Shift >= numObsPerInterval)
		throw "Shift " + toString(Shift) + " outside interval[0, " + toString(numObsPerInterval) + "]!";

	//initialize
	initialize(numActivityIntervals, numObsPerInterval, Shift);

	//read K
	if(params.parameterExists("K")){
		std::vector<double> K_vec(numActivityIntervals_nh);
		params.fillParameterIntoVector("K", K_vec, ',');
		setK(K_vec);
	}

	//report
	logfile->list("Will use " + toString(numActivityIntervals_nh) + " activity intervals of " + toString(activityLength / 60) + " min each.");
	logfile->list("Will use " + toString(numObservationIntervals_n) + " observation intervals (" + toString(numObservationPerActivityInterval) + " per activity interval) of " + toString(observationlength / 60) + " min each.");
	logfile->list("Will shift the observation intervals by " + toString(shift) + ".");
	std::string K_string = toString(K[0].value);
	for(int k=1; k<numActivityIntervals_nh; ++k)
		K_string += ", " + toString(K[k].value);
	logfile->list("Set K = " + K_string + ".");

	logfile->endIndent();
};

TTimeIntervals::TTimeIntervals(std::vector<std::string> & mcmcFileHeader){
	initialize(mcmcFileHeader);
};

TTimeIntervals::~TTimeIntervals(){
	if(initialized){
		delete[] K;
		delete[] K_scaled;
		delete[] observationToActivityMap;
	}
};

void TTimeIntervals::initialize(int NumActivityIntervals, int NumObsPerInterval, int Shift){
	numActivityIntervals_nh = NumActivityIntervals;
	if(86400 % numActivityIntervals_nh != 0)
		throw "Number of seconds per day is not a multiple of number of activity intervals!";

	numObservationPerActivityInterval = NumObsPerInterval;
	numObservationIntervals_n = NumObsPerInterval * NumActivityIntervals;
	if(86400 % numObservationIntervals_n != 0)
		throw "Number of seconds per day is not a multiple of number of observation intervals!";

	//calculate length in seconds
	activityLength = 86400 / numActivityIntervals_nh;
	observationlength = 86400 / numObservationIntervals_n;

	//initialize K
	K = new TMCMCParameterPositive[numActivityIntervals_nh];
	K_scaled = new double[numActivityIntervals_nh];
	setKUniform();

	//initialize observation to activity map
	observationToActivityMap = new short[numObservationIntervals_n];
	fillObservationToActivityMap(Shift);

	initialized = true;
	curK = 0;
	logPriorProposalRatio = 0.0;
};

void TTimeIntervals::initialize(std::vector<std::string> & mcmcFileHeader){
	numSecondsPerDay = 24 * 60 * 60;

	//parse header: LL, shift_in_XX_seconds, K1, K2, ..., Kn
	int NumActivityIntervals = mcmcFileHeader.size() - 2;

	std::vector<std::string> vec;
	fillVectorFromString(mcmcFileHeader[1], vec, '_');

	int obsLength = stringToInt(vec[2]);
	int NumObsPerInterval = numSecondsPerDay / NumActivityIntervals / obsLength;

	initialize(NumActivityIntervals, NumObsPerInterval, stringToInt(vec[0]));
};

void TTimeIntervals::setReferenceDate(time_t time){
	struct tm* timeinfo = localtime(&time);
	timeinfo->tm_hour = 0;
	timeinfo->tm_min = 0;
	timeinfo->tm_sec = 0;

	referenceDate = mktime(timeinfo);
};

void TTimeIntervals::setK(std::vector<double> K_vec){
	if(K_vec.size() != (size_t) numActivityIntervals_nh)
		throw "Wrong number of K values provided: expect " + toString(numActivityIntervals_nh) + " but got " + toString(K_vec.size()) + "!";

	for(int k=0; k<numActivityIntervals_nh; ++k)
		K[k] = K_vec[k];

	normalizeK();
	fillKScaled();
};

void TTimeIntervals::normalizeK(){
	double sum = 0.0;
	for(int k=0; k<numActivityIntervals_nh; ++k)
		sum += K[k].value;

	//normalize such that sum = numActivityIntervals_nh
	//this implies that K = 1.0 is lambda_bar
	double scale = (double) numActivityIntervals_nh / sum;
	for(int k=0; k<numActivityIntervals_nh; ++k)
		K[k].scale(scale);
};

void TTimeIntervals::setKUniform(){
	for(int k=0; k<numActivityIntervals_nh; ++k)
		K[k] = 1.0;
	fillKScaled();
};

void TTimeIntervals::fillKScaled(){
	for(int i=0; i<numActivityIntervals_nh; ++i)
		K_scaled[i] = K[i].value * observationlength;
};

void TTimeIntervals::fillObservationToActivityMap(int Shift){
	shift = Shift % numObservationPerActivityInterval;
	for(int i=0; i<numObservationIntervals_n; ++i){
		int index = i - shift;
		if(index < 0) index = numObservationIntervals_n + index;
		observationToActivityMap[i] = (index / numObservationPerActivityInterval) % numActivityIntervals_nh;
	}
};

void TTimeIntervals::setFromMcmcLine(std::vector<double> & mcmcLine){
	//first column is LL, second shift
	shift = mcmcLine[1];

	//rest are K
	for(int k=0; k<numActivityIntervals_nh; ++k)
		K[k] = mcmcLine[k+2];

	normalizeK();
	fillKScaled();
};

double TTimeIntervals::calcLogLikelihood(const double & lambdaBar, const double & p, int*** data_sums){
	double LL = 0;
	if (shift > numObservationPerActivityInterval) shift -= numObservationPerActivityInterval;

	for(int i=0; i<numActivityIntervals_nh; ++i){
		//calculate lambda for activity interval
		double lambda = lambdaBar * p * K_scaled[i];

		//P(Y=0|theta) = exp(-lambda) -> log P(Y=1|theta) = -lambda;
		//P(Y=1|theta) = 1.0 - P(Y=0|theta) -> log P(Y=1|theta) = log(1.0 - exp(-lambda))
		//TODO! Create lookup table to interpolate?
		int sum_zero = data_sums[0][shift][i];
		int sum_one = data_sums[1][shift][i];

		if(lambda > 1E-15)
			LL += log(1.0 - exp(-lambda)) * sum_one - lambda * sum_zero;
		else
			LL += -34.53958 * sum_one - lambda * sum_zero; //put minimum to avoid underflow.
	}

	return LL;
};

double TTimeIntervals::getK(const double timeOfDayFraction){
	int seconds = timeOfDayFraction * numSecondsPerDay - shift;
	if(seconds < 0)
		seconds = numSecondsPerDay + seconds;

	int index = (double) seconds / (double) numSecondsPerDay * numActivityIntervals_nh;
	return K[index].value;
};


int TTimeIntervals::getObservationInterval(time_t time){
	return ((time - referenceDate) % numSecondsPerDay ) / observationlength;
};

int TTimeIntervals::getNextObservationInterval(time_t time){
	int secOfDay = (time - referenceDate) % numSecondsPerDay;
	if(secOfDay % observationlength == 0)
		return secOfDay / observationlength;
	else
		return secOfDay / observationlength + 1;
};

time_t TTimeIntervals::getStartOfObservationInterval(time_t day, int interval){
	int numDays = (day - referenceDate) / numSecondsPerDay;
	return referenceDate + numDays * numSecondsPerDay + interval * observationlength;
};

std::string TTimeIntervals::getKString(){
	std::string ret = toString(K[0].value);
	for(int k=1; k<numActivityIntervals_nh; ++k)
		ret += ", " + toString(K[k].value);
	return ret;
};

bool TTimeIntervals::updateNextK(TRandomGenerator* randomGenerator){
	//restart?
	if(curK == numActivityIntervals_nh)
		curK = 0;
	else {
		++curK;
		if(curK == numActivityIntervals_nh)
			return false;
	}

	//update curK
	logPriorProposalRatio = K[curK].update(randomGenerator);
	if(K[curK].value > numActivityIntervals_nh){
		//reject update
		K[curK].reject();
		return updateNextK(randomGenerator);
	};

	//get sum of K
	double sum = 0.0;
	for(int k=0; k<numActivityIntervals_nh; ++k)
		sum += K[k].value;

	//normalize all expect curK such that sum = numActivityIntervals_nh
	//this implies that K = 1.0 is lambda_bar
	double scale = ((double) numActivityIntervals_nh - K[curK].value) / (sum - K[curK].value);
	for(int k=0; k<numActivityIntervals_nh; ++k){
		if(k != curK) K[k].scale(scale);
	}

	//fill scaled
	fillKScaled();

	return true;
};

void TTimeIntervals::rejectLastUpdate_K(){
	for(int k=0; k<numActivityIntervals_nh; ++k){
		if(k == curK) K[k].reject();
		else K[k].reset();
	}

	fillKScaled();
};

void TTimeIntervals::updateShift(TRandomGenerator* randomGenerator){
	shift_old = shift;

	if(randomGenerator->getRand() < 0.5)
		shift = (shift + 1) % numObservationPerActivityInterval;
	else {
		shift -= 1;
		if(shift < 0)
			shift = numObservationPerActivityInterval - shift;
	}
	logPriorProposalRatio = 0.0;
	++numShiftUpdates;
	++numShiftUpdatesAccepted;
};

void TTimeIntervals::rejectShitUpdate(){
	shift = shift_old;
	--numShiftUpdatesAccepted;
};

void TTimeIntervals::adjustProposalRanges(){
	for(int k=0; k<numActivityIntervals_nh; ++k)
		K[k].adjustProposal();

	numShiftUpdates = 0;
	numShiftUpdatesAccepted = 0;
};

void TTimeIntervals::writeMCMCHeader(gz::ogzstream & out){
	out << "LL\tshift_in_" << observationlength << "_seconds";
	for(int k=0; k<numActivityIntervals_nh; ++k)
		out << "\tK_" << k+1;
	out << "\n";
};

void TTimeIntervals::writeMCMCValues(gz::ogzstream & out, const double LL){
	out << LL << "\t" << shift;
	for(int k=0; k<numActivityIntervals_nh; ++k)
		out << "\t" << K[k].value;
	out << "\n";
};

void TTimeIntervals::printAcceptanceRates(TLog* logfile){
	std::string s = toString(K[0].getAcceptanceRate());
	for(int k=1; k<numActivityIntervals_nh; ++k)
		s += ", " + toString(K[k].getAcceptanceRate());
	logfile->list("K = " + s);
	logfile->list("shift = " + toString((double) numShiftUpdatesAccepted / (double) numShiftUpdates));
};
