/*
 * TDeltaEstimator.cpp
 *
 *  Created on: Mar 25, 2019
 *      Author: phaentu
 */

#include "TDeltaTEstimator.h"

#include "TMCMCFile.h"
#include "TFile.h"
#include "TTimeIntervals.h"

TDeltaTEstimator::TDeltaTEstimator(TLog* Logfile){
	logfile = Logfile;
	numPoints = 0;
	points = NULL;
	initialized = false;
};

TDeltaTEstimator::~TDeltaTEstimator(){
	if(initialized){
		delete[] points;
	}
};

std::string TDeltaTEstimator::initializeDeltaTEstimation(TParameters & params){
	//open mcmc files
	std::string filename1 = params.getParameterString("mcmc_K");
	if(filename1.empty()) throw "Please provide a valid filename with mcmc_K.";
	mcmcFile1.open(filename1);

	std::string filename2 = params.getParameterString("mcmc_K_2");
	if(filename2.empty()) throw "Please provide a valid filename with mcmc_K_2.";
	mcmcFile2.open(filename2);

	//initialize time objects
	intervals1.initialize(mcmcFile1.header);
	intervals2.initialize(mcmcFile2.header);

	//prepare time points to calc delta
	numPoints = params.getParameterIntWithDefault("points", 2400);
	logfile->list("Will evaluate deltaT at " + toString(numPoints) + " points throughout the day.");
	points = new double[numPoints];
	for(int i=0; i<numPoints; ++i)
		points[i] = (double) i / (double) numPoints;

	//open output file
	std::string outPrefix = params.getParameterString("out", false);
	if(outPrefix == ""){
		//construct output from observation file name
		outPrefix = filename1;
		outPrefix = extractBefore(outPrefix, '.');
		std::string tmp = filename2;
		tmp = extractBefore(tmp, '.');
		outPrefix += "_" + tmp;
	}
	return outPrefix;
};

bool TDeltaTEstimator::readNext(){
	if(mcmcFile1.readNext() && mcmcFile2.readNext()){
		//set parameters in intervals
		intervals1.setFromMcmcLine(mcmcFile1.values);
		intervals2.setFromMcmcLine(mcmcFile2.values);
		return true;
	} else return false;
};

double TDeltaTEstimator::calculateCurrentDeltaT(){
	double delta = 0.0;
	for(int i=0; i<numPoints; ++i){
		delta += std::min(intervals1.getK(points[i]), intervals2.getK(points[i]));
	}

	delta /= (double) numPoints;
	meanVar.add(delta);
	return delta;
};

double TDeltaTEstimator::calculateCurrentDeltaTWeighted(double weight1, double weight2){
	double delta = 0.0;
	for(int i=0; i<numPoints; ++i){
		delta += std::min(intervals1.getK(points[i]) * weight1, intervals2.getK(points[i]) * weight2);
	}

	return delta / (double) numPoints;
};

void TDeltaTEstimator::printMeanVar(){
	meanVar.calcMeanVar();
	logfile->conclude("Mean deltaT = " + toString(meanVar.mean()));
	logfile->conclude("Variance deltaT = " + toString(meanVar.var()));
};

void TDeltaTEstimator::estimateDeltaT(TParameters & params){
	//open MCMC files
	std::string outPrefix = initializeDeltaTEstimation(params);

	//open output file
	std::string outFileName = outPrefix + "_deltaT.txt.gz";
	logfile->list("Will write delta posterior to '" + outFileName + "'.");
	TOutputFileZipped out(outFileName);
	out.writeHeader({"deltaT"});

	//limit lines
	int limitLines = params.getParameterIntWithDefault("limitLines", 100000000);
	logfile->startIndent("Comparing densities at up to " + toString(limitLines) + " posterior samples:");
	logfile->listOverFlush("Parsed 0 posterior samples ...");

	//parse through mcmc files
	while(mcmcFile1.getNumSamplesRead() < limitLines && readNext()){
		//calculate and write overlap
		out << calculateCurrentDeltaT() << std::endl;

		//report
		if(mcmcFile1.getNumSamplesRead() % 100 == 0)
			logfile->listOverFlush("Parsed " + toString(mcmcFile1.getNumSamplesRead()) + " posterior samples ...");
	}

	logfile->overList("Parsed " + toString(mcmcFile1.getNumSamplesRead()) + " posterior samples ... reached end of file.");

	//conclude
	meanVar.calcMeanVar();
	logfile->conclude("Mean deltaT = " + toString(meanVar.mean()));
	logfile->conclude("Variance deltaT = " + toString(meanVar.var()));

	//clean
	delete[] points;
};


