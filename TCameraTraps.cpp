/*
 * TCameraTrap.cpp
 *
 *  Created on: Dec 15, 2018
 *      Author: phaentu
 */

#include "TCameraTraps.h"

//--------------------------------------------
// TCameraTrap
//--------------------------------------------
TCameraTrap::TCameraTrap(){
	init();
};

void TCameraTrap::init(){
	start = 0;
	end = 0;
	hasData = false;
	numObservationIntervals = 0;
	data = NULL;
	LL = 0.0;
	LL_old = 0.0;
};

TCameraTrap::TCameraTrap(const time_t Start, const time_t End){
	init();
	initialize(Start, End);
};

TCameraTrap::~TCameraTrap(){
	if(hasData){
		delete[] rawObervationCounts;
		delete[] data[0];
		delete[] data[1];
		delete[] data;
	}
};

void TCameraTrap::initialize(const time_t & Start, const time_t & End){
	//set start and end time
	start = Start;
	end = End;
};

void TCameraTrap::setEnvironmentalVariables_lambda(std::vector<double> & Env, TEnvironment* env_lambda){
	lambdaBar.init(Env, env_lambda);
};

void TCameraTrap::setEnvironmentalVariables_p(std::vector<double> & Env, TEnvironment* env_p){
	p.init(Env, env_p);
};

void TCameraTrap::addToMeanVar(TMeanVar* mv_lambda, TMeanVar* mv_p){
	lambdaBar.addToMeanVar(mv_lambda);
	p.addToMeanVar(mv_p);
};

void TCameraTrap::standardizeEnv(TMeanVar* mv_lambda, TMeanVar* mv_p){
	lambdaBar.standardize(mv_lambda);
	p.standardize(mv_p);
};

void TCameraTrap::updateLambdaBar(TEnvironment* env){
	lambdaBar.update(env);
};

void TCameraTrap::updateP(TEnvironment* env){
	p.update(env);
};

double TCameraTrap::calcLogLikelihood(TTimeIntervals & intervals){
	if(hasData){
		LL_old = LL;
		LL = intervals.calcLogLikelihood(lambdaBar.value, p.value, data_sums);
		return LL;
	}
	return 0.0;
};

void TCameraTrap::resetLambdaBar(){
	LL = LL_old;
	lambdaBar.reset();
};

void TCameraTrap::resetP(){
	LL = LL_old;
	p.reset();
};

void TCameraTrap::write(const std::string & name, gz::ogzstream & out, TEnvVar & envVar){
	//name
	out << name;

	//start and end date / time
	out << "\t" << timeToString(start) << "\t" << timeToString(end);

	//environment
	envVar.write(out);
	out << "\n";
};

void TCameraTrap::write(const std::string & name, gz::ogzstream & out_lambda, gz::ogzstream & out_p){
	write(name, out_lambda, lambdaBar);
	write(name, out_p, p);
};

void TCameraTrap::initializeDataStorage(TTimeIntervals & intervals){
	hasData = true;

	//find first observation interval
	int obsIndexStart = intervals.getNextObservationInterval(start);
	startOfFirstInterval = intervals.getStartOfObservationInterval(start, obsIndexStart);
	numObservationIntervals = (end - startOfFirstInterval) / intervals.getObservationIntervalLength() + 1;
	endOfLastInterval = startOfFirstInterval + numObservationIntervals * intervals.getObservationIntervalLength();

	//create raw count storage
	rawObervationCounts = new uint8_t[numObservationIntervals];
	for(int i=0; i<numObservationIntervals; ++i)
		rawObervationCounts[i] = 0;

	//then 0/1 data used in MCMC
	data = new uint8_t*[2];
	data[0] = new uint8_t[intervals.getNumObservationIntervals()];
	data[1] = new uint8_t[intervals.getNumObservationIntervals()];
	for(int i=0; i<intervals.getNumObservationIntervals(); ++i){
		data[0][i] = 0;
		data[1][i] = 0;
	}

	data_sums = new int**[2];
	data_sums[0] = new int*[intervals.getNumObservationsPerActivityInterval()];
	data_sums[1] = new int*[intervals.getNumObservationsPerActivityInterval()];
	for(int i = 0; i < intervals.getNumObservationsPerActivityInterval(); i++) {
		data_sums[0][i] = new int[intervals.getNumActivityIntervals()];
		data_sums[1][i] = new int[intervals.getNumActivityIntervals()];

		for (int j = 0; j < intervals.getNumActivityIntervals(); j++) {
			data_sums[0][i][j] = 0;
			data_sums[1][i][j] = 0;
		}
	}
};

bool TCameraTrap::addObservation(time_t time, TTimeIntervals & intervals){
	if(time >= startOfFirstInterval && time < endOfLastInterval)
		++rawObervationCounts[(time - startOfFirstInterval) / intervals.getObservationIntervalLength()];
	else if(time < start || time > end)
		return false;

	return true;
};

void TCameraTrap::compileData(TTimeIntervals & intervals){
	for(int i=0; i<numObservationIntervals; ++i){
		int index = intervals.getObservationInterval(startOfFirstInterval + i * intervals.getObservationIntervalLength());

		if(rawObervationCounts[i] > 0)
			++data[1][index];
		else
			++data[0][index];
	}

	// Cache values for each shift
	for(int shift = 0; shift < intervals.getNumObservationsPerActivityInterval(); shift++) {
		for (int j = 0; j < intervals.getNumActivityIntervals(); j++) {
			for(int k = 0; k < intervals.getNumObservationsPerActivityInterval(); ++k){
				int index = (shift + j * intervals.getNumObservationsPerActivityInterval() + k) % intervals.getNumObservationIntervals();
				data_sums[0][shift][j] += data[0][index];
				data_sums[1][shift][j] += data[1][index];
			}
		}
	}
};

void TCameraTrap::writeData(const std::string & name, gz::ogzstream & out, TTimeIntervals & intervals){
	for(int i=0; i<numObservationIntervals; ++i){
		//name
		out << name;

		//time of interval
		out << "\t" << timeToString(startOfFirstInterval + i * intervals.getObservationIntervalLength());

		//counts
		out << "\t" << (int) rawObervationCounts[i] << "\n";
	}
};

int TCameraTrap::simulateObservationsInterval(const std::string & name, const time_t & intervalStart, const double & intervalLength, const double & lambda, gz::ogzstream & out, TRandomGenerator* randomGenerator){
	int numEvents = 0;
	double nextEvent = randomGenerator->getExponentialRandom(lambda);
	while(nextEvent < intervalLength){
		//write observation
		out << name << "\t" << timeToString(intervalStart + nextEvent) << "\n";
		++numEvents;

		//add time to next event
		nextEvent += randomGenerator->getExponentialRandom(lambda);
	}
	return numEvents;
};

int TCameraTrap::simulateObservations(const std::string & name, TTimeIntervals & intervals, gz::ogzstream & out, TRandomGenerator* randomGenerator){
	//get minutes of day of start and end
	struct tm* timeinfo;
	timeinfo = localtime(&start);
	int secOfDay_start = timeinfo->tm_hour * 3600 + timeinfo->tm_min * 60 + timeinfo->tm_sec;

	//simulate first observation interval: might be just part of a bin!
	double tmp = secOfDay_start / intervals.getObservationIntervalLength();
	int intervalIndex = floor(tmp);
	double firstLength = (intervalIndex + 1) * intervals.getObservationIntervalLength() - secOfDay_start;
	double lambda = lambdaBar.value * p.value * intervals.getKObservationInterval(intervalIndex);
	int numEvents = simulateObservationsInterval(name, start, firstLength, lambda, out, randomGenerator);

	//simulate all others
	time_t intervalStart = start + firstLength;
	time_t relavantEnd = end - intervals.getObservationIntervalLength();
	while(intervalStart < relavantEnd){
		intervalIndex = (intervalIndex  + 1) % intervals.getNumObservationIntervals();

		//simulate
		lambda = lambdaBar.value * p.value * intervals.getKObservationInterval(intervalIndex);
		numEvents += simulateObservationsInterval(name, intervalStart, intervals.getObservationIntervalLength(), lambda, out, randomGenerator);

		//move to next interval
		intervalStart += intervals.getObservationIntervalLength();
	}

	//simulate last: might be just part of a bin!
	double lastLength = end - intervalStart;
	if(lastLength > 0.0){
		intervalIndex = (intervalIndex  + 1) % intervals.getNumObservationIntervals();
		lambda = lambdaBar.value * p.value * intervals.getKObservationInterval(intervalIndex);
		numEvents += simulateObservationsInterval(name, intervalStart, lastLength, lambda, out, randomGenerator);
	}

	return numEvents;
};

int TCameraTrap::getTotalObservations(){
	int numObs = 0;

	for(int i=0; i<numObservationIntervals; ++i)
		numObs += rawObervationCounts[i];

	return numObs;
};

//--------------------------------------------
// TCameraTraps
//--------------------------------------------
TCameraTraps::TCameraTraps(TEnvironment_lambda* Env_lambda, TEnvironment_p* Env_p, TTimeIntervals* Intervals){
	intervals = Intervals;
	env_lambda = Env_lambda;
	env_p = Env_p;

	//traps
	numTraps = 0;

	//LL
	LL = 0.0;
	LL_old = 0.0;
};

TCameraTraps::~TCameraTraps(){
	for(const auto& t : traps)
		delete t.second;
};

void TCameraTraps::normalizeEnvVariables(std::string meanVarFileName_lambda, std::string meanVarFileName_p, TLog* logfile){
	//calculate mean and standard deviation
	TMeanVar* mv_lambda = new TMeanVar[env_lambda->size()];
	TMeanVar* mv_p = new TMeanVar[env_p->size()];

	//add not container
	for(const auto& t : traps)
		t.second->addToMeanVar(mv_lambda, mv_p);

	//calc mean and var
	for(size_t i=0; i<env_lambda->size(); ++i){
		mv_lambda[i].calcMeanVar();

		if(mv_lambda[i].sd() == 0.0){
			mv_lambda[i].setMeanSD(mv_lambda[i].mean(), 1.0);
			logfile->warning("No variation in environmental variable '" + env_lambda->getName(i) + "'!");
		}
	}

	for(size_t i=0; i<env_p->size(); ++i){
		mv_p[i].calcMeanVar();

		if(mv_p[i].sd() == 0.0){
			mv_p[i].setMeanSD(mv_p[i].mean(), 1.0);
			logfile->warning("No variation in environmental variable '" + env_p->getName(i) + "'!");
		}
	}

	//write mean and std to file so that the same standardization can be done when projecting
	gz::ogzstream meanVar_lambda(meanVarFileName_lambda.c_str());
	if(!meanVar_lambda) throw "Failed to open file '" + meanVarFileName_lambda + "' for writing";
	meanVar_lambda << "EnvironmentalVariable\tMean\tSD\n";
	for(size_t i=0; i<env_lambda->size(); ++i)
		meanVar_lambda << env_lambda->getName(i) << "\t" << mv_lambda[i].mean() << "\t" << mv_lambda[i].sd() << "\n";
	meanVar_lambda.close();

	gz::ogzstream meanVar_p(meanVarFileName_p.c_str());
	if(!meanVar_p) throw "Failed to open file '" + meanVarFileName_p + "' for writing";
	meanVar_p << "EnvironmentalVariable\tMean\tSD\n";
	for(size_t i=0; i<env_p->size(); ++i)
		meanVar_p << env_p->getName(i) << "\t" << mv_p[i].mean() << "\t" << mv_p[i].sd() << "\n";
	meanVar_p.close();

	//now standardize
	for(const auto& t : traps)
		t.second->standardizeEnv(mv_lambda, mv_p);

	delete[] mv_lambda;
	delete[] mv_p;
};

bool TCameraTraps::updateLambdaBar(TRandomGenerator* randomGenerator){
	LL_old = LL;
	LL = 0.0;

	for(const auto& t : traps){
		t.second->updateLambdaBar(env_lambda);
		LL += t.second->calcLogLikelihood(*intervals);
	}

	if(!std::isfinite(LL)){
		//throw "Likelihood out of range!";

		//or just reject?
		resetLambdaBar();
		return false;
	}

	//accept or reject?
	double log_h = (LL - LL_old) + env_lambda->getLogPriorProposalRatioLastUpdate();

	if(log(randomGenerator->getRand()) > log_h){
		resetLambdaBar();
		return false;
	} else return true;
};

bool TCameraTraps::updateP(TRandomGenerator* randomGenerator){
	LL_old = LL;
	LL = 0.0;

	for(const auto& t : traps){
		t.second->updateP(env_p);
		LL += t.second->calcLogLikelihood(*intervals);
	}

	if(!std::isfinite(LL)){
		//throw "Likelihood out of range!";

		//or just reject?
		resetLambdaBar();
		return false;
	}

	//accept or reject?
	double log_h = (LL - LL_old) + env_p->getLogPriorProposalRatioLastUpdate();
	if(log(randomGenerator->getRand()) > log_h){
		resetP();
		return false;
	} else return true;
};

double TCameraTraps::update(){
	LL_old = LL;
	LL = 0.0;
	for(const auto& t : traps){
		t.second->updateLambdaBar(env_lambda);
		t.second->updateP(env_p);
		LL += t.second->calcLogLikelihood(*intervals);
	}

	if(!std::isfinite(LL)) throw "Likelihood out of range!";
	return LL;
};

void TCameraTraps::resetLambdaBar(){
	LL = LL_old;
	for(const auto& t : traps)
		t.second->resetLambdaBar();
};

void TCameraTraps::resetP(){
	LL = LL_old;
	for(const auto& t : traps)
		t.second->resetP();
};

bool TCameraTraps::updateK(TRandomGenerator* randomGenerator){
	LL_old = LL;
	LL = 0.0;
	for(const auto& t : traps){
		LL += t.second->calcLogLikelihood(*intervals);
	}

	if(!std::isfinite(LL)){
		//throw "Likelihood out of range!";

		//or just reject?
		resetLambdaBar();
		return false;
	}

	//accept or reject?
	double log_h = (LL - LL_old) + intervals->getLogPriorProposalRatioLastUpdate();
	if(log(randomGenerator->getRand()) > log_h){
		LL = LL_old;
		return false;
	} else {
		return true;
	}
};

void TCameraTraps::openFileForReading(std::istream* in, const std::string & filename, std::string tag, TLog* logfile){
	if(filename.find(".gz") == std::string::npos){
		logfile->listFlush(tag + " from file '" + filename + "' ...");
		in = new std::ifstream(filename.c_str());
	} else {
		logfile->listFlush(tag + " from gzipped file '" + filename + "' ...");
		in = new gz::igzstream(filename.c_str());
	}
};

int TCameraTraps::readHeaderOFCameraTrapFile(std::istream* in, std::vector<std::string> & envNames){
	std::vector<std::string> vec;
	std::string line;
	std::getline(*in, line);
	fillVectorFromStringAnySkipEmpty(line, vec, "\t\f\v\n\r");
	if(vec.size() < 4)
			throw "Expecting at least 4 columns: name of camera trap, start date/time, end date/time followed by environmental variables!";

	envNames.assign(vec.begin() + 3, vec.end());

	//return number of columns
	return vec.size();
};

void TCameraTraps::readTraps(const std::string & filename_lambda, const std::string & filename_p, TLog* logfile){
	// 1) for lambda
	//open file
	TCameraTrapfile file(filename_lambda, "Reading camera traps and environmental variables for lambda", logfile);

	//initialize environment
	env_lambda->initialize(file.envNames);

	//now read line by line and create traps
	while(file.readNext()){
		//check if camera trap exists already exists
		std::map<std::string, TCameraTrap*>::iterator it = traps.find(file.trapName);
		if(it != traps.end()) throw "Duplicate camera trap '" + file.trapName + "' " + file.getLineString() + "!";

		//check if dates are OK
		if(file.trapEnd <= file.trapStart){
			logfile->warning("Start >= end for camera trap '" + file.trapName + "' " + file.getLineString() + "! Ignoring this camera trap.");
			continue;
		}

		//initialize trap (why does emplace_back into vector<TCameraTrap> with arguments not work??)
		it = traps.emplace(file.trapName, new TCameraTrap(file.trapStart, file.trapEnd)).first;
		it->second->setEnvironmentalVariables_lambda(file.envVariables, env_lambda);
	}
	numTraps = traps.size();
	logfile->done();
	logfile->conclude("Read a total of " + toString(numTraps) + " camera traps with " + toString(env_lambda->size()) + " environmental variables for lambda each.");
	if(numTraps < 1) throw "At least one camera trap must be provided!";


	// 2) for p
	//open file
	file.open(filename_p, "Reading environmental variables for p", logfile);

	//initialize environment
	env_p->initialize(file.envNames);

	std::vector<std::string> noMatch;

	//now read line by line and create traps
	while(file.readNext()){
		//check if camera trap exists already exists
		std::map<std::string, TCameraTrap*>::iterator it = traps.find(file.trapName);
		if(it == traps.end()){
			noMatch.emplace_back(file.trapName);
			continue;
		}

		//check if times match
		if(it->second->getStart() != file.trapStart || it->second->getEnd() != file.trapEnd)
			throw "Camera trap '" + file.trapName + "' " + file.getLineString() + " has different start or end than in file '" + filename_lambda + "'!";

		//save environmental variables
		it->second->setEnvironmentalVariables_p(file.envVariables, env_p);
	}

	//report reading p
	logfile->done();
	logfile->conclude("Added " + toString(env_p->size()) + " environmental variables for p each to each camera trap.");

	//report missing
	if(noMatch.size() > 0){
		logfile->startIndent("The following camera traps were ignored as they were not present in file '" + filename_lambda + "':");
		for(std::vector<std::string>::iterator it = noMatch.begin(); it != noMatch.end(); ++it)
			logfile->list(*it);
		logfile->endIndent();
	}

	//check if a camera trap lacks environmental variables for p
	for(const auto& t : traps){
		if(!t.second->hasEnvironmentalVariables_p())
			throw "No environmental variables for p were provided for camera trap '" + t.first + "'!";
	}

	//set reference date
	time_t first = traps.begin()->second->getStart();
	for(const auto& t : traps){
		if(t.second->getStart() < first)
			first = t.second->getStart();
	}
	intervals->setReferenceDate(first);
};

void TCameraTraps::simulateTraps(int NumTraps, int Duration, TRandomGenerator* randomGenerator){
	numTraps = NumTraps;

	//chose start time
	time_t start = time(0);
    struct tm* tm = localtime(&start);
    tm->tm_mday += Duration;
    time_t end = mktime(tm);

	//simulate environment for each trap
	std::vector<double> envVar;
	for(int t=0; t<numTraps; ++t){
		//simulate environment
		env_lambda->simulateEnvironmentalVariables(envVar, randomGenerator);

		//initialize trap (why does emplace_back with arguments not work??)
		std::string name = "Trap_" + toString(t+1);
		std::map<std::string, TCameraTrap*>::iterator it = traps.emplace(name, new TCameraTrap(start, end)).first;
		it->second->setEnvironmentalVariables_lambda(envVar, env_lambda);

		//add env variables for p
		env_p->simulateEnvironmentalVariables(envVar, randomGenerator);
		it->second->setEnvironmentalVariables_p(envVar, env_p);
	}
};

void TCameraTraps::readObservations(const std::string & filename, TLog* logfile){
	//open file
	std::istream* in;
	if(filename.find(".gz") == std::string::npos){
		logfile->listFlush("Reading observations from file '" + filename + "' ...");
		in = new std::ifstream(filename.c_str());
	} else {
		logfile->listFlush("Reading observations from gzipped file '" + filename + "' ...");
		in = new gz::igzstream(filename.c_str());
	}

	//initialize storage
	for(const auto& t : traps)
		t.second->initializeDataStorage(*intervals);

	//now read line by line
	long lineNum = 0;
	std::vector<std::string> vec;
	std::string line;
	long numObs = 0;

	while(in->good() && !in->eof()){
		++lineNum;
		std::getline(*in, line);
		fillVectorFromStringAnySkipEmpty(line, vec, "\t\f\v\n\r"); //split by tabs only, not space as this is used in time

		//skip empty lines
		if(vec.size() == 0) continue;

		//check if there are three columns: trap date and time
		if(vec.size() != 2)
			logfile->warning("Wrong number of columns in file '" + filename + "' on line " + toString(lineNum) + "! Expected 2 (name of camera trap, date / time), but found " + toString(vec.size()) + ".");

		//check if camera trap exists exists
		std::map<std::string, TCameraTrap*>::iterator it = traps.find(vec[0]);
		if(it == traps.end()) continue;

		if(vec.size() != 2)
			throw "Can not add observation without date/time!";

		//add data to trap
		++numObs;
		if(!it->second->addObservation(stringToTime(vec[1]), *intervals))
			logfile->warning("Observation " + vec[1] + " outside operating range of camera trap '" + vec[0] + "'. Ignoring this observation.");
	}
	//close file
	delete in;

	//compile data
	for(const auto& t : traps)
		t.second->compileData(*intervals);

	//report
	logfile->done();
	logfile->conclude("Read a total of " + toString(numObs) + " observations.");
	if(numObs < 1) throw "Need at least one obervation!";
};

int TCameraTraps::simulateObservations(std::string outname, TRandomGenerator* randomGenerator){
	//open file
	gz::ogzstream out(outname.c_str());
	if(!out) throw "Failed to open file '" + outname + "' for writing!";

	//simulate (no header)
	int numObservations = 0;
	for(const auto& t : traps)
		numObservations += t.second->simulateObservations(t.first, *intervals, out, randomGenerator);

	//close file
	out.close();

	//return num observations simulated
	return numObservations;
};

void TCameraTraps::writeTraps(const std::string filename_lambda, const std::string filename_p){
	//open files
	gz::ogzstream out_lambda(filename_lambda.c_str());
	if(!out_lambda) throw "Failed to open file '" + filename_lambda + "' for writing!";

	gz::ogzstream out_p(filename_p.c_str());
	if(!out_lambda) throw "Failed to open file '" + filename_p + "' for writing!";

	//write header
	out_lambda << "Trap\tStart\tEnd\t" << env_lambda->getNames("\t") << "\n";
	out_p << "Trap\tStart\tEnd\t" << env_p->getNames("\t") << "\n";

	//write traps
	for(const auto& t : traps)
		t.second->write(t.first, out_lambda, out_p);

	out_lambda.close();
	out_p.close();
};

double TCameraTraps::getFractionOfIntervalsWithObservations(){
	//guess lambdaBar by counting
	int numObs = 0;
	int numIntervals = 0;
	for(const auto& t : traps){
		numObs += t.second->getTotalObservations();
		numIntervals += t.second->getNumObservationIntervals();
	}

	return (double) numObs / (double) numIntervals;
};









