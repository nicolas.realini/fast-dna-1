/*
 * TDeltaEstimator.h
 *
 *  Created on: Mar 25, 2019
 *      Author: phaentu
 */

#ifndef TDELTATESTIMATOR_H_
#define TDELTATESTIMATOR_H_

#include "TLog.h"
#include "TParameters.h"
#include "TMeanVar.h"
#include "TMCMCFile.h"
#include "TTimeIntervals.h"

class TDeltaTEstimator{
private:
	TLog* logfile;

	int numPoints;
	double* points;
	bool initialized;

	TMCMCFile mcmcFile1;
	TMCMCFile mcmcFile2;

	TTimeIntervals intervals1;
	TTimeIntervals intervals2;

	TMeanVar meanVar;

public:
	TDeltaTEstimator(TLog* Logfile);
	~TDeltaTEstimator();

	std::string initializeDeltaTEstimation(TParameters & params);
	bool readNext();
	double calculateCurrentDeltaT();
	double calculateCurrentDeltaTWeighted(double weight1, double weight2);
	void printMeanVar();

	void estimateDeltaT(TParameters & params);
};


#endif /* TDELTATESTIMATOR_H_ */
