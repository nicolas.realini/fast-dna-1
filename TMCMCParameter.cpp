/*
 * TMCMCParameter.cpp
 *
 *  Created on: Dec 19, 2018
 *      Author: phaentu
 */

#include "TMCMCParameter.h"


//-------------------------------------------
// TPrior
//-------------------------------------------
TPriorUniform::TPriorUniform(){
	min = 0.0;
	max = 1.0;
	logDensity = 0.0;
};

TPriorUniform::TPriorUniform(double Min, double Max){
	initialize(Min, Max);
};

void TPriorUniform::initialize(double Min, double Max){
	min = Min;
	max = Max;
	if(min >= max) throw "Can not initialize uniform prior with min >= max!";
	logDensity = log(1.0 / (max - min));
};

double TPriorUniform::getLogPriorDensity(double x){
	if(x < min || x > max) return -std::numeric_limits<double>::infinity();
	return logDensity;
};

double TPriorUniform::getRandomValue(TRandomGenerator* randomGenerator){
	return randomGenerator->getRand(min, max);
};

//----------------------------------------------
TPriorNormal::TPriorNormal(){
	initialize(0.0, 1.0);
};

TPriorNormal::TPriorNormal(double Mean, double Sd){
	initialize(Mean, Sd);
};

void TPriorNormal::initialize(double Mean, double Sd){
	mean = Mean;
	sd = Sd;

	twoSd = 2.0 * sd;
	logOneOverSqrtTwoPiSigma = -log(sqrt(2.0 * 3.1415928 * sd));
};

double TPriorNormal::getLogPriorDensity(double x){
	double tmp = (x-mean);
	return logOneOverSqrtTwoPiSigma - tmp * tmp / twoSd;
};

double TPriorNormal::getRandomValue(TRandomGenerator* randomGenerator){
	return randomGenerator->getNormalRandom(mean, sd);
};


//----------------------------------------------
TPriorExponential::TPriorExponential(){
	initialize(1.0);
};

TPriorExponential::TPriorExponential(double Lambda){
	initialize(Lambda);
};

void TPriorExponential::initialize(double Lambda){
	lambda = Lambda;
	logLambda = log(lambda);
};

double TPriorExponential::getLogPriorDensity(double x){
	return logLambda - lambda * x;
};

double TPriorExponential::getRandomValue(TRandomGenerator* randomGenerator){
	return randomGenerator->getExponentialRandom(lambda);
};


//-------------------------------------------
// TMCMCParameter
//-------------------------------------------
TMCMCParameter::TMCMCParameter(){
	value = 0.0;
	value_old = 0.0;
	updates = 0;
	acceptedUpdates = 0;
	propSD = 0.1;
	prior = NULL;
	hasPrior = false;
};

TMCMCParameter::TMCMCParameter(double val){
	value = val;
	value_old = val;
	updates = 0;
	acceptedUpdates = 0;
	propSD = 0.1;
	prior = NULL;
	hasPrior = false;
};

double TMCMCParameter::update(TRandomGenerator* & randomGenerator){
	value_old = value;
	value = randomGenerator->getNormalRandom(value, propSD);
	++acceptedUpdates;
	++updates;

	//return log(proposal ratio) + log(prior ratio)
	if(hasPrior){
		return prior->getLogPriorDensity(value) - prior->getLogPriorDensity(value_old);
	} else {
		return 0.0;
	}
};

void TMCMCParameter::reject(){
	value = value_old;
	--acceptedUpdates;
};

void TMCMCParameter::adjustProposal(){
	if(updates > 0){
		//get scaling
		double acc = getAcceptanceRate();
		double scale = 3.0 * acc;
		if(scale < 0.1) scale = 0.1;
		if(scale > 10) scale = 10;

		//update
		propSD *= 3.0 * acc;
		updates = 0;
		acceptedUpdates = 0;
	}
};

double TMCMCParameter::getAcceptanceRate(){
	return ((double) acceptedUpdates + 1.0) / ((double) updates + 1.0);
};

//-------------------------------------------
// TMCMCParameterPositive
//-------------------------------------------
double TMCMCParameterPositive::update(TRandomGenerator* & randomGenerator){
	TMCMCParameter::update(randomGenerator);

	//now mirror
	if(value < 0.0) value = fabs(value);

	//return log(proposal ratio) + log(prior ratio)
	//Here, log(proposal ratio) is 0.0 as this update is symmetric
	if(hasPrior)
		return prior->getLogPriorDensity(value) - prior->getLogPriorDensity(value_old);
	else return 0.0;
};

//-------------------------------------------
// TMCMCParameterZeroOne
//-------------------------------------------
double TMCMCParameterZeroOne::update(TRandomGenerator* & randomGenerator){
	TMCMCParameter::update(randomGenerator);

	//now mirror
	if(value < 0.0) value = fabs(value);
	else if(value > 1.0) value = 2.0 - value;

	return 0.0; //log(proposal ratio) is 0.0 as this update is symmetric
};

void TMCMCParameterZeroOne::adjustProposal(){
	TMCMCParameter::adjustProposal();
	//make sure it is not too large
	if(propSD > 0.25) propSD = 0.25;
};

//-------------------------------------------
// TMCMCParameterMixture
//-------------------------------------------
TMCMCParameterMixture::TMCMCParameterMixture():TMCMCParameter(){
	isZero = false;
	isZero_old = false;
	numWasZero = 0;
	probJumpToZero = 0.0;
	logProbJumpToZero = 0.0;
	priorJumpAwayFromZero = NULL;
	pi = NULL;
};

TMCMCParameterMixture::TMCMCParameterMixture(double val):TMCMCParameter(val){
	isZero = false;
	isZero_old = false;
	numWasZero = 0;
	probJumpToZero = 0.1;
	logProbJumpToZero = log(probJumpToZero);
	priorJumpAwayFromZero = NULL;
	pi = NULL;
};

void TMCMCParameterMixture::setJumpToZero(TMCMCParameter* Pi, double probabilityToJump, TPrior* PriorToJump){
	pi = Pi;
	probJumpToZero = probabilityToJump;
	logProbJumpToZero = log(probJumpToZero);
	priorJumpAwayFromZero = PriorToJump;
};

void TMCMCParameterMixture::set(double val){
	if(val == 0.0){
		isZero = true;
		value = 0.0;
	} else {
		isZero = false;
		value = val;
	}
};

double TMCMCParameterMixture::update(TRandomGenerator* & randomGenerator){
	//pi is probability P(non-zero) in favor of a model with A != 0
	value_old = value;
	isZero_old = isZero;

	//choose type
	if(isZero == true){
		//jump to non-zero
		isZero = false;
		value = priorJumpAwayFromZero->getRandomValue(randomGenerator);

		//return log(proposal ratio) + log(prior ratio)
		//contains the relative probability to make these updates (1.0 vs probJumpToZero)
		//and the density to propose the specific value
		//and the prior P(non-zero) / P(zero)
		//and the prior density
		double tmp = -logProbJumpToZero - priorJumpAwayFromZero->getLogPriorDensity(value) + log(pi->value / (1.0 - pi->value));
		if(hasPrior){
			return tmp + prior->getLogPriorDensity(value);
		}
		else return tmp;

	} else {
		//update
		if(randomGenerator->getRand() < probJumpToZero){
			//jump to zero
			isZero = true;
			value = 0.0;

			//return log(proposal ratio) + log(prior ratio)
			//contains the relative probability to make these updates (1.0 vs probJumpToZero)
			//and the density to propose the specific value
			//and the prior P(zero) / P(non-zero)
			//and the prior density
			double tmp = logProbJumpToZero + priorJumpAwayFromZero->getLogPriorDensity(value_old) + log((1.0 - pi->value)/pi->value);
			if(hasPrior)
				return tmp - prior->getLogPriorDensity(value_old);
			else return tmp;

		} else {
			//update regularly
			value = randomGenerator->getNormalRandom(value, propSD);
			++updates;
			++acceptedUpdates;

			//return log(proposal ratio) + log(prior ratio)
			//Here, log(proposal ratio) is 0.0 as this update is symmetric
			if(hasPrior)
				return prior->getLogPriorDensity(value) - prior->getLogPriorDensity(value_old);
			else return 0.0;
		}
	}
};

void TMCMCParameterMixture::reject(){
	value = value_old;

	if(isZero == isZero_old)
		--acceptedUpdates;
	else
		isZero = isZero_old;
};


