#!/bin/zsh
# This script computes the task "estimate" on the upstream branch for different seed values (set in the "seeds" array)
# and compares them with the codebase found in the origin master branch
# IMPORTANT: To be able to run this script, the upstream branch must be correctly set 
# (git remote add upstream https://bitbucket.org/wegmannlab/tomcat.git)

# Call the program with the "update" parameter (./test.sh update) to update the upstream repository and recompute the results

# TODO: Also compute time difference between upstream and origin
# TODO: fix ctrl-c trap

readonly log_file="/dev/null"

readonly build_dir="./cmake-build-debug"
readonly exe_name="./TomCat"
readonly full_exe=$build_dir/$exe_name 

readonly tests_dir="tests"
readonly original_prefix="reference_" # these folders will contain the original (correct) results
readonly new_prefix="new_"            # these folders will contain the new (to be tested) results

trap "
    #git checkout master
    #git stash pop
    exit
" INT TERM # same as below, for shells other than bash (optionally add "ERR" to exit on script error)
trap "
    #git checkout master
    #git stash pop
    kill 0
" EXIT # terminate all background (child) jobs on CTRL-C

seeds=(10 20 30 40 50 0)

# FUNCTIONS 

# Execute "estimate" task with different fixed seeds in parallel
# execute_tests (dir)
function execute_tests () {
    dir="$1";
    if [ ! -f $full_exe ]; then
        >&2 echo "ERROR: TomCat executable not found. Pease build it first.";
        exit 1;
    fi

    cd $build_dir;

    if [ ! -d $tests_dir ]; then
	echo "INFO: Tests dir doesn't exist, creating: $tests_dir"
        mkdir $tests_dir;
    fi

    cd $tests_dir;
    for seed in $seeds; do
        rm -rf $dir$seed;
        mkdir $dir$seed;
        cd $dir$seed;
        echo "INFO: Running estimate task with seed $seed...";
        ../../$exe_name \
              task=estimate \
              verbose \
              traps_lambda=../../Tomcat_simulation_cameraTraps_lambda.txt.gz \
              traps_p=../../Tomcat_simulation_cameraTraps_p.txt.gz \
              observations=../../Tomcat_simulation_observations.txt.gz \
              numBurnin=4 \
              burnin=1000 \
              iterations=10000 \
              fixedSeed=$seed \
              &> /dev/null &;
        cd ..;
    done

    wait $(jobs -p) # wait for all jobs to be done

    echo "INFO: All jobs done.";
    cd ../..;
}

# MAIN
if [[ $1 == "update" ]]; then
    echo "INFO: Getting latest upstream and creating reference test data...";
    # get lastest upstream and create reference tests
    git stash save "test script" &> $log_file; # stash unsaved changes
    git checkout upstream/master &> $log_file; # switch branch
    git pull upstream master &> $log_file;     # update upstream
    
    # check we're in the correct branch
    if [[ ! $(git rev-parse --abbrev-ref HEAD) == "master" ]]; then
	echo "INFO: Compiling upstream...";
	g++ -O3 -std=c++11 -o $build_dir/TomCat *.cpp -lz;
	execute_tests $original_prefix;
        rm -rf /tmp/$tests_dir;
        mv -f $build_dir/$tests_dir /tmp;
    else
        >&2 echo "ERROR: Couldn't switch to upstream, exiting.";
        git switch - &> $log_file;
        git stash pop &> $log_file;
        return;
    fi

    git checkout master &> $log_file;   # switch to my branch
    git stash pop &> $log_file;         # restore unsaved changes
    mv -f /tmp/$tests_dir $build_dir;
fi

if [[ $1 != "time" ]]; then
    echo "INFO: Compiling origin..."
    #make clean -C $build_dir > $log_file;
    make -C $build_dir > $log_file;
    execute_tests $new_prefix;

# unzip everything
gzip -df $build_dir/$tests_dir/*/*.gz

# Check (diff) new_ files with corresponding original_ files
echo "INFO: Testing file equality (diff)"
cd $build_dir/$tests_dir;
tests_passed=0;
for seed in $seeds; do
    if [ ! -d $original_prefix$seed ]; then
        >&2 echo "ERROR: Original files with seed $seed not found!";
        continue;
    fi
    if [[ $(diff -x "*.prof" $original_prefix$seed $new_prefix$seed) == "" ]]; then
        ((tests_passed++));
    else
        echo "WARN: Files for seed $seed are not equal!";
    fi
done
cd ../..;

echo "INFO: Tests passed: $tests_passed/${#seeds[@]}"
fi;

seeds=(10);
git stash save "test script" &> $log_file;
git checkout upstream/master &> $log_file;
echo "INFO: Compiling upstream...";
g++ -O3 -std=c++11 -o $build_dir/TomCat *.cpp -lz &> $log_file;
echo "INFO: Timing upstream with seed 10...";
{ time (execute_tests $original_prefix); };
git switch - &> $log_file;
git stash pop &> $log_file;
echo "INFO: Compiling origin...";
make -C $build_dir &> $log_file;
echo "INFO: Timing origin with seed 10...";
{ time (execute_tests $new_prefix); };
