/*
 * TSimulator.cpp
 *
 *  Created on: Dec 15, 2018
 *      Author: phaentu
 */

#include "TSimulator.h"

TSimulator::TSimulator(TLog* Logfile){
	logfile = Logfile;
};

void TSimulator::runSimulations(TParameters & params, TRandomGenerator* randomGenerator){
	logfile->startIndent("Reading simulation parameters:");

	//read parameters for traps
	//------------------------------
	int numTraps = params.getParameterIntWithDefault("traps", 100);
	int durationDays = params.getParameterIntWithDefault("days", 100);
	logfile->list("Will simulate " + toString(numTraps) + " camera traps that run for " + toString(durationDays) + " days each.");
	int numLocations = params.getParameterIntWithDefault("locations", 0);
	if(numLocations > 0)
		logfile->list("Will simulate " + toString(numLocations) + " locations to project densities.");

	//initialize environment: lambda
	//------------------------------
	TEnvironment_lambda env_lambda;
	logfile->startIndent("Environment regarding lambda:");

	//num variables
	std::vector<int> numEnv;
	params.fillParameterIntoVector("env_lambda", numEnv, ',');
	if(numEnv.size() == 1) numEnv.push_back(0);
	else if(numEnv.size() > 2) throw "Expect only one or two comma delimited values for 'env_lambda'!";
	logfile->list("Simulating " + toString(numEnv[0] + numEnv[1]) + " environmental variables, of which " + toString(numEnv[0]) + " affect lambda.");

	//a0
	double a0_lambda = params.getParameterDoubleWithDefault("a0_lambda", 0.0);
	logfile->list("Will use a0 = " + toString(a0_lambda) + ".");

	//Two possibilities for A: either an sd to simulate from a nornal distribution (default), or provided by user
	if(params.parameterExists("A_lambda")){
		//A are provided
		std::vector<double> A;
		params.fillParameterIntoVector("A_lambda", A, ',');
		if(A.size() != numEnv[0])
			throw "Number of A_lambda values provided (" + toString(A.size()) + ") does not match number of environmental variables affecting lambda (" + toString(numEnv[0]) + ")!";
		env_lambda.simulate(numEnv[0], numEnv[1], a0_lambda, A, randomGenerator);
		logfile->list("Will use A = " + env_lambda.getString_A());
	} else {
		//simulate A from normal distribution
		double sd_A_lambda = params.getParameterDoubleWithDefault("sd_A_lambda", 0.5);
		logfile->listFlush("Simulating A_i ~ N(0, " + toString(sd_A_lambda) +") ...");
		env_lambda.simulate(numEnv[0], numEnv[1], a0_lambda, sd_A_lambda, randomGenerator);
		logfile->done();
		logfile->conclude("Simulated A = " + env_lambda.getString_A());
	}
	logfile->endIndent();

	//initialize environment: p
	//------------------------------
	TEnvironment_p env_p;
	logfile->startIndent("Environment regarding p:");

	//num variables
	params.fillParameterIntoVector("env_p", numEnv, ',');
	if(numEnv.size() == 1) numEnv.push_back(0);
	else if(numEnv.size() > 2) throw "Expect only one or two comma delimited values for 'env_p'!";
	logfile->list("Simulating " + toString(numEnv[0] + numEnv[1]) + " environmental variables, of which " + toString(numEnv[0]) + " affect p.");

	//Two possibilities for A: either an sd to simulate from a nornal distribution (default), or provided by user
	if(params.parameterExists("A_p")){
		//A are provided
		std::vector<double> A;
		params.fillParameterIntoVector("A_p", A, ',');
		if(A.size() != numEnv[0])
			throw "Number of A_p values provided (" + toString(A.size()) + ") does not match number of environmental variables affecting p (" + toString(numEnv[0]) + ")!";
		env_p.simulate(numEnv[0], numEnv[1], A, randomGenerator);
		logfile->list("Will use A = " + env_lambda.getString_A());
	} else {
		double sd_A_p = params.getParameterDoubleWithDefault("sd_A_p", 0.5);
		logfile->listFlush("Simulating A_i ~ N(0, " + toString(sd_A_p) +") ...");
		env_p.simulate(numEnv[0], numEnv[1], sd_A_p, randomGenerator);
		logfile->done();
		logfile->conclude("Simulated A = " + env_p.getString_A());
	}
	logfile->endIndent();

	//initialize intervals
	//------------------------------
	TTimeIntervals intervals(params, logfile);
	intervals.setReferenceDate(time(NULL));
	logfile->endIndent();

	//output names
	//------------------------------
	std::string out = params.getParameterStringWithDefault("out", "Tomcat_simulation_");
	logfile->list("Will write output files with prefix '" + out + "'.");

	//simulate camera traps
	//------------------------------
	logfile->listFlush("Simulating " + toString(numTraps) + " camera traps ...");
	std::string filename_lambda = out + "cameraTraps_lambda.txt.gz";
	std::string filename_p = out + "cameraTraps_p.txt.gz";
	TCameraTraps traps(&env_lambda, &env_p, &intervals);

	traps.simulateTraps(numTraps, durationDays, randomGenerator);
	traps.writeTraps(filename_lambda, filename_p);
	logfile->done();
	logfile->conclude("Wrote camera traps and environmental variables for lambda to '" + filename_lambda + "'.");
	logfile->conclude("Wrote camera traps and environmental variables for p to '" + filename_p + "'.");

	//simulate observations
	//------------------------------
	logfile->listFlush("Simulating observations ...");
	std::string filename = out + "observations.txt.gz";
	traps.update();
	int numObs = traps.simulateObservations(filename, randomGenerator);
	logfile->done();
	logfile->conclude("Wrote " + toString(numObs) + " observations to file '" + filename + "'.");

	//simulate locations
	simulateLocations(out, numLocations, env_lambda, randomGenerator);
};

void TSimulator::simulateLocations(const std::string & out, const int & numLocations, TEnvironment_lambda & env_lambda, TRandomGenerator* randomGenerator){
	if(numLocations > 0){
		logfile->listFlush("Simulating locations ...");

		//open locus file and add header
		TOutputFileZipped locFile(out + "locations.txt.gz");
		std::vector<std::string> header = {"coor1", "coor2"};
		env_lambda.addNamesToVector(header);
		locFile.writeHeader(header);

		//open true density file and add header
		TOutputFileZipped trueDensFile(out + "trueDensities.txt.gz");
		trueDensFile.writeHeader({"coor1", "coor2", "density"});

		//simulate locations
		std::vector<double> envVar;
		double linearComb;
		for(int i=0; i<numLocations; ++i){
			//simulate environmental variables and get density
			env_lambda.simulateEnvironmentalVariables(envVar, randomGenerator);

			//write locations
			locFile << i << i;
			locFile.write(envVar);
			locFile.endLine();

			//write true density
			trueDensFile << i << i << env_lambda.getParamValue(envVar, linearComb) << std::endl;
		}

		logfile->done();
	}
};
